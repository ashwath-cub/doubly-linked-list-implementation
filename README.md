**A Simple Doubly Linked List Implementation**

## Instructions

1. Install the make utility.
2. Use the "make" command to generate the binary "test_dll" on the cmd line.
3. Look for the test results generated with the help of the Unity framework on stdout.
4. The file "test_dll.c" contains all the test code. The assert library by the unity framework is used to check function returns.
5. Look for the file "results.txt" which has the state of the list after every operation.
