/* 
 * Author:       Ashwath Gundepally, CU ECEE
 * 
 * File:         doubly_ll.c
 * 
 * Description:  Contains an implementation of a doubly linked list
 *               that supports basic operations like add node, remove
 *               node, search item, create list, size and dump list.
 * */

#include "doubly_ll.h"
#include<stdint.h>
#include<stdlib.h>
#include<stdio.h>


/*								                
 * Function:     dll_create_list(dll_node_ptr* new_head, uint32_t data)
 * -----------------------------------------------------------------------------
 * Description:  creates a new list given a double ptr to head and the data that 
 *               the head is supposed to contain. 
 *           
 * Usage:        Pass a pointer to the pointer to the head node of this new list.
 *               Also pass the data that the head is to contain.
 *
 * Returns:      Error codes:
 *               DLL_NULL_POINTER: The pointer passed is detected to be a 
 *               null. The function halts execution and returns w/o completion. 
 *        
 *               DLL_MALLOC_FAIL: The call to malloc fails.
 *
 *               DLL_SUCCESS: The funcion returns successfully.
 */
dll_code dll_create_list(dll_node_ptr* head, uint32_t data)
{
    /*error handling*/
    if(head==NULL)                                                                   
        return DLL_NULL_PTR;   
    /* allocate memory */
    (*head)=(dll_node_ptr)malloc(sizeof(dll_node));     	 
    
    /* check call to malloc */
    if((*head)==NULL)
	    return DLL_MALLOC_FAIL;
    
    /* this is now the head pointer, so the previous should be NULL */
	(*head)->prev_ptr=NULL;                          
	
    /* the older version of the head ptr is the next pointer for this linked list */
    (*head)->next_ptr=NULL;                          
	
    /* assign data */
    (*head)->data= data;                             
	
    /* return successfully */ 
    return DLL_SUCCESS;    
}

/*								                
 * Function:     dll_add_node(dll_node_ptr* head, uint32_t data, uint32_t position)
 * -----------------------------------------------------------------------------
 * Description:  Addes a new node to the dll with the head ptr *head at position 
 *               and also assigns data to the node.
 *           
 * Usage:        Pass a pointer to the pointer to the head node of the ll.
 *               If a non-null head is detected with position zero, a new node
 *               will be created at position zero. This new_node will be the
 *               new head.
 *
 * Notes:        Don't use this instead of create_list; notice how the next ptr 
 *               of the new node is the current node. Unexpected results may 
 *               occur if (*head) is not NULL when using this function 
 *               instead of create_list.
 *
 * Returns:      Error codes:
 *               DLL_NULL_POINTER: The pointer passed is detected to be a 
 *               null. The function halts execution and returns w/o completion. 
 *        
 *               DLL_MALLOC_FAIL: The call to malloc fails.
 *
 *               DLL_SUCCESS: The funcion returns successfully.
 */
dll_code dll_add_node(dll_node_ptr* head, uint32_t position, uint32_t data)
{
    /*error handling*/
    if(head==NULL)                                                          
        return DLL_NULL_PTR;   

    /* if (*head) is NULL, the list does not exist and is thus an illegal request */
    if((*head)==NULL)
	    return DLL_NULL_PTR;                                               

    /* This case helps to create a new head node to an already existing list */
    if(position==0)
    {  
        /* allocate memory */
        dll_node_ptr new_node=(dll_node_ptr)malloc(sizeof(dll_node));     	 

        /* check call to malloc */
        if(new_node==NULL)
	        return DLL_MALLOC_FAIL;
        
        /* this is now the head pointer, so the previous should be NULL */
	    new_node->prev_ptr=NULL;              

        /* the older version of the head ptr is the next pointer for this linked list */
	    new_node->next_ptr=*head;             

        /* assign data */
        new_node->data= data;                 

        /* link the older head to this new node before modifying the older head */
	    (*head)->prev_ptr=new_node;           

        /* position zero is always the head */
        *head=new_node;        

        /* return successfully */ 
	    return DLL_SUCCESS;
    }
    else
    {
        /* check if given position is valid */
        
        dll_node_ptr tmp_head=*head;
        
        uint32_t index, size;

        dll_code size_rc=dll_size(tmp_head, &size);
        
        /* only fail rc for dll_size */
        if(size_rc!=DLL_NULL_PTR)                            
        {
             /* the position cannot be 2 for a dll of size 2; position starts from 0 */
             if(position>size)                              
                 return DLL_BAD_POSITION;
        }    
        else
	        return DLL_NULL_PTR;

#ifdef DEBUG 
        printf("before traversing to p-1\n"); 
#endif
	    /*go to index=position-1 in the dll*/
	    for(index=0; index<position-1; index++)
            tmp_head=tmp_head->next_ptr; 

#ifdef DEBUG         
        printf("after traversing to p-1\n"); 
#endif
        /* allocate memory */
	    dll_node_ptr new_node=(dll_node_ptr)malloc(sizeof(dll_node));      
	 
	    /* malloc check */
	    if(new_node==NULL)
	        return DLL_MALLOC_FAIL;
         
	    new_node->data=data;

	    /* link the new node to the nodes before and after it in the dll */
	    new_node->prev_ptr=tmp_head;
	    new_node->next_ptr=tmp_head->next_ptr;
        
	    /* link the nodes surrounding the new node to the new node */ 
        tmp_head->next_ptr=new_node;
	 
	    if(new_node->next_ptr!=NULL)
	        (new_node->next_ptr)->prev_ptr=new_node;
#ifdef DEBUG 
        printf("after making all the assignments\n"); 
#endif
	    /* return successfully */
        return DLL_SUCCESS;                          
    }
}	


/*								                
 * Function:     dll_destroy(dll_node_ptr* head)
 * -----------------------------------------------------------------------------
 * Description:  De-allocates memory on the heap allocated to all the nodes in
 *               the doubly linked list.
 *               
 * Usage:        Pass a pointer the head of the dll.
 * 
 * Returns:      Error codes:
 *               DLL_NULL_PTR: The pointer passed to the function is a
 *               NULL and is thus invalid. The function halts execution and 
 *               returns.
 *
 *               DLL_SUCCESS: The function completes execution 
 *               completely.   
 * ----------------------------------------------------------------------------
 */
dll_code dll_destroy(dll_node_ptr head)
{
    //basic pointer check; error handling	
    if(head==NULL)
	 return DLL_NULL_PTR;

    /*assign the head to a tmp pointer*/
    dll_node_ptr tmp_head;
    
    /*free the memory of all the nodes on the heap*/
    do
    {
	    tmp_head=head;                           //assign current next node value to a temporary variable
	    free(tmp_head);                          //delete current node
        head=(head)->next_ptr;                   //go to next node
    }while(head!=NULL);
    
    /* return successfully */
    return DLL_SUCCESS; 
}




/*								                
 * Name:         dll_remove_node(dll_node_ptr* head, uint32_t position)
 * -----------------------------------------------------------------------------
 * Description:  Removes node from the dll safely at a given index.
 *               
 * Working:      Takes a ptr to the head and traverses the dll to reach
 *               position-1, grabs node at position and frees its memory. Links
 *               node before and after position safely. Returns node data in
 *               the pointer passed.
 * 
 * Returns:      Error codes:
 *               DLL_NULL_PTR: The pointer passed to the function is a
 *               NULL and is thus invalid. The function halts execution and
 *               returns.
 *
 *               DLL_BAD_POSITION: The dll's size is lesser than the position
 *               specified.
 *
 *               DLL_SUCCESS: The function completes execution successfully
 * ----------------------------------------------------------------------------
 */
dll_code dll_remove_node(dll_node_ptr* head, uint32_t position, uint32_t* data)
{
    /* basic pointer check; error handling */
    if(head==NULL)
	    return DLL_NULL_PTR;
    if(*head==NULL)
	    return DLL_NULL_PTR;

    /* assign head to temp variable */
    dll_node_ptr tmp_head=*head;                                                 

    /* handle position equals zero case */	 
    if(position==0)
    {   
        /* get node data in the input pointer parameter */
        *data=tmp_head->data;                   

        /* if the next is NULL, the size is 1 */
    	if((*head)->next_ptr!=NULL)                                               
	    {    
	        *head=(*head)->next_ptr;
	        (*head)->prev_ptr=NULL;
	    }

	    free(tmp_head);

	    /* return successfully */
        return DLL_SUCCESS;
    }
    
    /* check if given position is valid */
    uint32_t size;
    dll_code size_rc=dll_size(tmp_head, &size);
    
    /* only fail rc for dll_size */
    if(size_rc!=DLL_NULL_PTR)                                                   
    {
        /* the position cannot be 2 for a dll of size 2; position starts from 0 */
        if(position>size-1)                          
             return DLL_BAD_POSITION;
    }    
    else
	    return DLL_NULL_PTR;

    /* loop variable */
    uint32_t index;                                                             
    
    /*reach node of position-1*/
    for(index=0; index<position-1; index++)
         tmp_head=tmp_head->next_ptr;
    
    /* store the node to be deleted */
    dll_node_ptr delete_node=tmp_head->next_ptr;
    
    /* get node data in the input parameter */
    *data=delete_node->data;

    /* link the previous and the next node of the node to be deleted appropriately */
    tmp_head->next_ptr=delete_node->next_ptr;
    
    /* complete this only if the next node is not NULL */
    if(delete_node->next_ptr!=NULL)                                             
         (delete_node->next_ptr)->prev_ptr=tmp_head;
    
    /* de-allocate memory for node */
    free(delete_node);
    
    /* return successfully */
    return DLL_SUCCESS;
}

/*								                
 * Function:     dll_size(dll_node_ptr head, uint32_t* size)
 * -----------------------------------------------------------------------------
 * Description:  Returns size of the dll whose head is given by *head.
 *      
 * Usage:        Pass a pointer to the head of the dll, a pointer
 *               to a uint32_t type in that order. The pointer to uint32_t will 
 *               contain the size of the dll.
 *                
 * Returns:      Error codes:
 *               DLL_NULL_PTR: The pointer passed to the function is a
 *               NULL and is thus invalid. The function halts execution and 
 *               returns.
 *
 *               DLL_SUCCESS: The function completes execution successfully.   
 * ----------------------------------------------------------------------------
 */
dll_code dll_size(dll_node_ptr head, uint32_t* size)
{
    /* basic pointer check; error handling */
    if(size==NULL)
         return DLL_NULL_PTR;
    if(head==NULL)
    {
        *size=0;
	    return DLL_SUCCESS;
    }
    /* assign head to a temp variable */
    dll_node_ptr tmp_head=head;         
    
    /* initialise count to zero- this will track the dll's size */
    uint32_t count=0;                                                    
    
    /* increment count as long as the node is not a NULL */
    while(tmp_head!=NULL)           
    {
         tmp_head=tmp_head->next_ptr;
    	 count++;
    }  
    
    /* assign count to the size pointer */ 
    *size=count;                                                                
    
    /* return successfully */
    return DLL_SUCCESS;                                                         
}	


/*								                
 * Function:     dll_search(dll_node_ptr head, uint32_t data, uint32_t* position)
 * -----------------------------------------------------------------------------
 * Description:  Returns the position of the node containing the data input via a 
 *               input pointer of the dll with the head pointer equal to head.
 *               
 * Usage:        Checks to see if the data element is there in each node starting
 *               from head. Returns the position of the first node which has the
 *               data. Sets position pointer to NULL and passes apt return code
 *               to indicate data was not found.
 *               
 *
 * Returns:      Error codes:
 *               DLL_NULL_PTR: The pointer passed to the function is a
 *               NULL and is thus invalid. The function halts execution and 
 *               returns.
 *
 *               DLL_DATA_MISSING: The data requested to be searched was not
 *               found present in any of the nodes.
 *
 *               DLL_SUCCESS: The function completes execution 
 *               successfully- the data is found.
 * ----------------------------------------------------------------------------
 */
dll_code dll_search(dll_node_ptr head, uint32_t data, uint32_t* position)
{
    //basic pointer check; error handling	
    if(head==NULL||position==NULL)
        return DLL_NULL_PTR;
    
    /* assign the head variable to a temporary variable */
    dll_node_ptr tmp=head;                                                     
    
    /*this variable keeps track of the position*/
    uint32_t count=0;

#ifdef DEBUG_SEARCH
    printf("data requested:%u\n", data);
#endif
    /*check the entire list to see if data is found*/
    while(tmp!=NULL)
    {
        /* break out of the loop if data is found */ 
        if(tmp->data==data)                                                     
	        break;
#ifdef DEBUG_SEARCH
	printf("data:%u , position:%u\n", tmp->data,count);
#endif	
	    /* increment count to track position of data */
        count++;                                                               
        /* move to the next node */
	    tmp=tmp->next_ptr;                                                      
    }
    
    /* basically we reached the end of the dll- the break prevents this */
    if(tmp==NULL)                 
	    return DLL_DATA_MISSING;
    
    /* data is found- return position safely */
    *position=count;
    return DLL_SUCCESS;
}

/*								                
 * Function:     dll_dump(dll_node_ptr head, FILE* fp)
 * -----------------------------------------------------------------------------
 * Description:  Prints out all the elements of the linked list in a readable
 *               manner.
 *               
 * Usage:        Pass the pointer to the head node of the linked list to have
 *               all the data elements printed out sequentially.                
 *
 * Returns:      Error codes:
 *               DLL_NULL_PTR: The pointer passed to the function is a
 *               NULL and is thus invalid. The function prints message to stdout
 *               and returns.
 *
 *               DLL_SUCCESS: The function completes execution 
 *               successfully.
 * ----------------------------------------------------------------------------
 */
dll_code dll_dump(dll_node_ptr head, FILE *fp)
{
    /* NULL checks on params */
    if(head==NULL)
         return DLL_NULL_PTR;
    if(fp==NULL)
         return DLL_NULL_PTR;
     
    /* create temporary variable to traverse the dll */
    dll_node_ptr temp=head;
    
    /*print each data element in a readable manner until you reach NULL*/
    while(temp!=NULL)
    {
        fprintf(fp, "%u -> ",temp->data);
	    temp=temp->next_ptr;
    }

    /*end the printing with NULL*/ 
    fprintf(fp, "NULL\n");
    
    /*return successfully*/
    return DLL_SUCCESS;
}
