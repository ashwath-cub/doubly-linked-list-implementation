
 # Author:       Ashwath Gundepally, CU ECEE
 # 
 # File:         makefile
 # 
 # Description:  makefile for the Doubly Linked list repository.
 #
CFILES1= test_dll.c 
CFILES2= doubly_ll.c
CFILES3= Unity/src/unity.c

CC=gcc

CFLAGS=-c -Wall

all: test_dll

test_dll: test_dll.o doubly_ll.o unity.o
	$(CC) test_dll.o doubly_ll.o unity.o -o test_dll

test_dll.o: test_dll.c
	$(CC) $(CFLAGS) test_dll.c

doubly_ll.o: doubly_ll.c
	$(CC) $(CFLAGS) doubly_ll.c

unity.o: Unity/src/unity.c
	$(CC) $(CFLAGS) Unity/src/unity.c
clean:
	rm -rf *.o *.d *.txt test_dll
